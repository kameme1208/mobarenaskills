package kame.mobarenaskills.skill;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import kame.api.collision.ray.BlockRay;
import kame.api.collision.ray.Ray;
import kame.api.collision.ray.Ray.RayHit;
import kame.api.collision.ray.WorldRay;
import kame.api.nbt.TagSection;
import kame.kameplayer.baseutils.stand.BlockObject;
import kame.mobarenaskills.ArenaMetaValue;
import kame.mobarenaskills.Main;

public class Witch implements Listener {

	public static ArenaMetaValue createMeta(Player player, int require, int cost) {
		return new SkillMeta(player, require, cost);
	}
	
	@EventHandler
	private void onSkillEnable(PlayerDropItemEvent event) {
		Player player = event.getPlayer();
		if(!Main.hasSkill(player.getUniqueId(), SkillMeta.class))return;
		event.setCancelled(true);
		if(Main.isSkillEnabled(player.getUniqueId(), SkillMeta.class))return;
		Main.requestEnable(player, SkillMeta.class);
	}
	
	@EventHandler
	private void onSkillEnable(PlayerSwapHandItemsEvent event) {
		if(!Main.hasSkill(event.getPlayer().getUniqueId(), SkillMeta.class))return;
		
		List<Creature> monsters = event.getPlayer().getNearbyEntities(8, 4, 8)
				.stream()
				.filter(Creature.class::isInstance)
				.map(Creature.class::cast)
				.collect(Collectors.toList());
		
		for(Creature monster : monsters) {
			monster.damage(4, event.getPlayer());
			monster.setVelocity(new Vector(0, 1, 0));
			monster.getWorld().spawnParticle(Particle.SWEEP_ATTACK, monster.getEyeLocation(), 5);
			monster.getWorld().playSound(monster.getEyeLocation(), Sound.ENTITY_PLAYER_ATTACK_SWEEP, 1, 1.5f);
			new BukkitRunnable() {
				private int count;
				private Location loc = event.getPlayer().getEyeLocation();
				@Override
				public void run() {
					monster.damage(4, event.getPlayer());
					monster.getWorld().spawnParticle(Particle.SWEEP_ATTACK, monster.getEyeLocation(), 1);
					monster.getWorld().playSound(monster.getEyeLocation(), Sound.ENTITY_PLAYER_ATTACK_SWEEP, 1, 1.5f);
					monster.setGravity(false);
					monster.setVelocity(Vector.getRandom().multiply(0.1));
					if(count++ >= 5) {
						this.cancel();
						monster.setGravity(true);
						monster.setVelocity(monster.getLocation().subtract(loc).toVector().normalize());
					}
				}
			}.runTaskTimer(Main.instance, 10, 2);
		}
	}
	
	private static class SkillMeta extends ArenaMetaValue {
		
		protected SkillMeta(Player player, int require, int cost) {
			super("§eスキル§f発動待機中…→§aKey F", player, require, cost);
		}
		
		@Override
		public void onSkillEnable() {
			
		}
		
		@Override
		public void onSkillEnd() {
			
		}

		@Override
		public boolean onTick() {
			return super.onTick();
		}
		
	}
	
	@EventHandler
	private void onWitchInteract(PlayerInteractEvent event) {
		switch(event.getAction()) {
		default:return;
		case RIGHT_CLICK_AIR:
		case RIGHT_CLICK_BLOCK:
		}
		if(!event.hasItem())return;
		TagSection section = new TagSection(event.getItem()).getSection("ArenaWand", false);
		if(section == null)return;
		switch(section.get("Type", String.class, "fire").toLowerCase()) {
		case "fire":
			WitchFire(event.getPlayer());
			return;
		case "ice":
			WitchIce(event.getPlayer());
			return;
		case "thunder":
			WitchThunder(event.getPlayer());
			return;
		}
	}
	
	private void WitchFire(Player player) {
		Location loc = player.getEyeLocation();
		Vector vec = loc.getDirection().multiply(0.1);
		World world = loc.getWorld();
		Random rnd = new Random();
		Bukkit.getScheduler().runTaskTimer(Main.instance, () -> {
			world.spawnParticle(Particle.FLAME, loc.clone().add(rnd.nextGaussian(), rnd.nextGaussian(), rnd.nextGaussian()), 0, vec.getX(), vec.getY(), vec.getZ());
			for(Entity entity : world.getNearbyEntities(loc.add(vec), 0.1, 0.1, 0.1)) {
				if(entity instanceof Creature) {
					entity.setFireTicks(100);
					((Creature)entity).damage(15, player);
					return;
				}
			}
		}, 1, 1);
	}
	
	private void WitchIce(Player player) {
		Location loc = player.getEyeLocation();
		Vector vec = loc.getDirection().multiply(0.3);
		World world = loc.getWorld();
		new BukkitRunnable() {
			public void run() {
				Location l = loc.clone().setDirection(Vector.getRandom().subtract(Vector.getRandom()));
				BlockObject obj = new BlockObject(l, new ItemStack(Material.ICE));
				world.playSound(loc, Sound.ITEM_ARMOR_EQUIP_CHAIN, 2, 2);
				obj.show(loc.getWorld().getPlayers());
				Bukkit.getScheduler().runTaskLater(Main.instance, obj::hide, 20);
				for(Entity entity : world.getNearbyEntities(loc.add(vec), 0.3, 0.3, 0.3)) {
					if(entity instanceof Creature) {
						((Creature)entity).damage(15, player);
						return;
					}
				}
				if(!BlockRay.from(l, loc).useTemplateFilter().trace().isEmpty())this.cancel();
			}
		}.runTaskTimer(Main.instance, 1, 1);
	}
	
	private void WitchThunder(Player player) {
		Ray ray = WorldRay.from(player).useTemplateFilter().trace();
		for(RayHit hit : ray) {
			Location l = hit.getLocation();
			l.getWorld().strikeLightningEffect(l);
			l.getWorld().spawnParticle(Particle.EXPLOSION_LARGE, l, 1);
			l.getWorld().getNearbyEntities(l, 1, 2, 1);
			for(Entity entity : l.getWorld().getNearbyEntities(l.add(0, 1, 0), 1, 2, 1)) {
				if(entity instanceof Creature) {
					((Creature)entity).damage(10, player);
				}
			}
			for(int ii = 0; ii < 3; ii++){
				Location loc = hit.getLocation().subtract(0, 4, 0);
				for(int i = 0; i < 3; i++) {
					Vector vec = Vector.getRandom().add(new Vector(0, 5, 0));
					loc.setDirection(vec);
					thunderParticle(loc.clone(), loc.add(vec));
					
				}
			}
		}
		
	}
	
	private void thunderParticle(Location from, Location to) {
		World world = from.getWorld();
		
		while(from.add(from.getDirection().multiply(0.1)).distanceSquared(to) > 0.1)
		{
			world.spawnParticle(Particle.FIREWORKS_SPARK, from, 0, 0, -0.1, 0);
		}
	}

}
