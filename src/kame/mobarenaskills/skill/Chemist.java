package kame.mobarenaskills.skill;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerAnimationEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import kame.api.collision.ray.BlockRay;
import kame.api.collision.ray.WorldRay;
import kame.api.nbt.TagReader;
import kame.api.nbt.TagTool;
import kame.api.particle.BlockObject;
import kame.mobarenaskills.ArenaMetaValue;
import kame.mobarenaskills.Main;

public class Chemist implements Listener {

	public static ArenaMetaValue createMeta(Player player, int require, int cost) {
		return new SkillMeta(player, require, cost);
	}
	
	@EventHandler
	private void onSkillEnable(PlayerSwapHandItemsEvent event) {
		Player player = event.getPlayer();
		if(!Main.hasSkill(player.getUniqueId(), SkillMeta.class))return;
		if(!TagTool.hasTag(event.getMainHandItem(), "arenabook"))return;
		event.setCancelled(true);
		if(Main.isSkillEnabled(player.getUniqueId(), SkillMeta.class))return;
		Main.requestEnable(player, SkillMeta.class);
	}
	
	@EventHandler
	private void onSkillUse(PlayerAnimationEvent event) {
		Player player = event.getPlayer();
		if(!Main.isSkillEnabled(player.getUniqueId(), SkillMeta.class))return;
		SkillMeta meta = Main.getSkill(player.getUniqueId(), SkillMeta.class);
		if(++meta.count == meta.stands.length)meta.count = 0;
		int select = meta.count;
		Location start = meta.stands[select].getLocation();
		meta.attacking[select] = true;
		
		Location loc = player.getEyeLocation().add(player.getEyeLocation().getDirection().multiply(32));
		
		WorldRay ray = WorldRay.from(player)
				.setDistance(32)
				.useTemplateFilter()
				.trace();
		
		
		Location end = !ray.isEmpty() ? loc = ray.iterator().next().getLocation() : loc;
		
		loc.getWorld().spawnParticle(Particle.CRIT_MAGIC, loc, 10);
		start.getWorld().playSound(start, Sound.ENTITY_WITHER_SHOOT, 1, 1);
		
		double r = loc.distance(player.getEyeLocation()) / 5;
		Location target = loc = loc.getWorld().getNearbyEntities(loc, r, r, r)
			.stream()
			.filter(Creature.class::isInstance)
			.map(LivingEntity.class::cast)
			.filter(LivingEntity::isValid)
			.map(LivingEntity::getEyeLocation)
			.sorted((x, y) -> Double.compare(x.distance(end), y.distance(end)))
			.findFirst().orElse(end).add(player.getEyeLocation().getDirection());
		
		loc = loc.subtract(0, 0.5, 0).clone();
		Vector vec = loc.subtract(start).toVector().normalize();
		
		new BukkitRunnable() {
			private int count;
			@Override
			public void run() {
				if(start.distance(target) <= 1 || count > 0) {
					start.setYaw(start.getYaw() + 40);
					meta.stands[select].teleport(start.add(vec.clone().multiply(0.1)));
					if(count++ > 5) {
						this.cancel();
						meta.attacking[select] = false;
					}
				}else {
					start.setYaw(start.getYaw() + 30);
					meta.stands[select].teleport(start.add(vec));
				}
			}
		}.runTaskTimer(Main.instance, 0, 0);
	}
	

	
	@EventHandler
	private void onChemistInteract(PlayerInteractEvent event) {
		switch(event.getAction()) {
		default:return;
		case RIGHT_CLICK_AIR:
		case RIGHT_CLICK_BLOCK:
		}
		if(!event.hasItem())return;
		TagReader reader = new TagReader(event.getItem());
		if(!reader.hasTag("MagicRod", Number.class))return;
		if(event.getPlayer().getGameMode() != GameMode.CREATIVE) {
			int cost = reader.get("MagicRod", Number.class, 1).intValue();
			if(event.getPlayer().getTotalExperience() < cost) { return; }
			event.getPlayer().giveExp(-cost);
			if(event.getPlayer().getExp() < 0) {
				int exp = event.getPlayer().getTotalExperience();
				event.getPlayer().setLevel(0);
				event.getPlayer().setTotalExperience(0);
				event.getPlayer().setExp(0);
				event.getPlayer().giveExp(exp);
			}
		}
		if(event.getPlayer().isSneaking()) {
			ChemistIce(event.getPlayer());
		}else {
			ChemistFire(event.getPlayer());
		}
	}
	
	private void ChemistFire(Player player) {
		Location loc = player.getEyeLocation();
		Vector vec = loc.getDirection().multiply(0.5);
		World world = loc.getWorld();
		Random rnd = new Random();
		new BukkitRunnable() {
			private int count;
			private Set<Entity> attacked = new HashSet<>();
			public void run() {
				world.spawnParticle(Particle.FLAME, loc.clone().add(rnd.nextGaussian() * 0.5, rnd.nextGaussian() * 0.5, rnd.nextGaussian() * 0.5), 0, vec.getX(), vec.getY(), vec.getZ());
				world.playSound(loc, Sound.BLOCK_FIRE_AMBIENT, 1, 2);
				for(Entity entity : world.getNearbyEntities(loc.add(vec), 0.5, 0.5, 0.5)) {
					if(entity instanceof Creature && attacked.add(entity)) {
						((Creature)entity).damage(8, player);
						count+=10;
					}
				}
				if(count++ > 50) this.cancel();
			}
		}.runTaskTimer(Main.instance, 1, 1);
	}

	private void ChemistIce(Player player) {
		Location loc = player.getEyeLocation();
		Vector vec = loc.getDirection().multiply(0.5);
		loc.add(vec);
		World world = loc.getWorld();
		Random rnd = new Random();
		new BukkitRunnable() {
			private int count;
			private Set<Entity> attacked = new HashSet<>();
			public void run() {
				Location old = loc.setDirection(Vector.getRandom().subtract(Vector.getRandom())).clone();
				for(Entity entity : world.getNearbyEntities(loc.add(vec), 0.5, 0.5, 0.5)) {
					if(entity instanceof Creature && attacked.add(entity)) {
						((Creature)entity).damage(7, player);
						((Creature)entity).addPotionEffect(PotionEffectType.SLOW.createEffect(100, 5), false);
						world.playSound(entity.getLocation(), Sound.BLOCK_GLASS_BREAK, 1, (float)Math.random() + 0.5f);
						count+=10;
					}
				}
				BlockObject obj = new BlockObject(loc.clone().add(rnd.nextGaussian() * 0.2, rnd.nextGaussian() * 0.2, rnd.nextGaussian() * 0.2), new ItemStack(Material.ICE));
				world.playSound(loc, Sound.ITEM_ARMOR_EQUIP_CHAIN, 1, (float)Math.random() * 1.5f + 0.5f);
				obj.show(world.getPlayers());
				Bukkit.getScheduler().runTaskLater(Main.instance, obj::hide, 20);
				if(count++ > 50) this.cancel();
				if(!BlockRay.from(old, loc).useTemplateFilter().trace().isEmpty())this.cancel();
			}
		}.runTaskTimer(Main.instance, 1, 1);
	}
	
	private static class SkillMeta extends ArenaMetaValue {
		
		private ArmorStand[] stands = new ArmorStand[5];
		private Location loc = new Location(null, 0, 0, 0);
		private boolean[] attacking = new boolean[stands.length];

		private byte count;
		protected SkillMeta(Player player, int require, int cost) {
			super("§1浮遊玉§f発射…→§aKey §bLeftClick", player, require, cost);
		}
		
		private ArmorStand createStand() {
			ArmorStand stand = player.getWorld().spawn(player.getLocation(), ArmorStand.class);
			stand.setVisible(false);
			stand.setMarker(true);
			stand.setInvulnerable(true);
			stand.setSmall(true);
			stand.setGravity(false);
			stand.getEquipment().setHelmet(new ItemStack(Material.GOLD_PLATE));
			return stand;
		}
		
		@Override
		public void onSkillEnable() {
			for(int i = 0; i < stands.length; i++)stands[i] = createStand();
			for(int i = 0; i < stands.length; i++)attacking[i] = false;
		}
		
		@Override
		public void onSkillEnd() {
			for(ArmorStand stand : stands)stand.remove();
		}
		
		@Override
		public boolean onTick() {
			loc.setYaw(loc.getYaw() + 5);
			for(ArmorStand stand : stands)stand.setFireTicks(10);
			for(int i = 0; i < stands.length; i++) {
				
				loc.setYaw(loc.getYaw() + 360 / stands.length);
				Location loc = stands[i].getLocation();
				stands[i].getNearbyEntities(.5, 1, .5).stream().filter(LivingEntity.class::isInstance).filter(x -> !(x instanceof Player || x instanceof ArmorStand))
				.map(LivingEntity.class::cast).forEach(x -> {
					x.setNoDamageTicks(0);
					x.damage(3);
					x.getWorld().playSound(x.getLocation(), Sound.BLOCK_GLASS_BREAK, 10, 2);
					x.getWorld().spawnParticle(Particle.SWEEP_ATTACK, loc.clone().add(0, 1, 0), 1);
				});
				if(enable % 3 == i && (stands[i].getLocation().distance(player.getLocation()) > 5|| attacking[i])) {
					stands[i].getWorld().playSound(stands[i].getEyeLocation(), Sound.ENTITY_PLAYER_ATTACK_SWEEP, 1, 0.5f);
					stands[i].getWorld().spawnParticle(Particle.SPELL_WITCH, stands[i].getEyeLocation().add(0, 1, 0), 1, 0, 0, 0, 0);
				}
				
				if(attacking[i])continue;
				Vector vec = player.getLocation().add(this.loc.getDirection().multiply(1.5)).subtract(loc).toVector();
				if(vec.length() > 1)vec.normalize();
				loc.setYaw(loc.getYaw() + 10);
				stands[i].teleport(loc.add(vec));
			}
			return super.onTick();
		}
		
	}
}
