package kame.mobarenaskills.skill;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.IntStream;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerAnimationEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.material.MaterialData;
import org.bukkit.util.Vector;

import kame.api.nbt.TagTool;
import kame.mobarenaskills.ArenaMetaValue;
import kame.mobarenaskills.Main;

public class Archer implements Listener {
	
	
	static {
		Bukkit.getScheduler().runTaskTimer(Main.instance, Archer::onTick, 1, 1);
	}
	
	private static Set<Impact> impacters = new HashSet<>();
	private static void onTick() {
		impacters.removeIf(Impact::impact);
	}

	public static ArenaMetaValue createMeta(Player player, int require, int cost) {
		return new SkillMeta(player, require, cost);
	}
	
	@EventHandler
	private void onSkillEnable(PlayerSwapHandItemsEvent event) {
		if(!TagTool.hasTag(event.getMainHandItem(), "arenabook"))return;
		Player player = event.getPlayer();
		if(!Main.hasSkill(player.getUniqueId(), SkillMeta.class))return;
		event.setCancelled(true);
		if(Main.isSkillEnabled(player.getUniqueId(), SkillMeta.class))return;
		Main.requestEnable(player, SkillMeta.class);
	}
	
	private Set<Arrow> arrows = new HashSet<>();
	@EventHandler
	private void onAnimation(PlayerAnimationEvent event) {
		if(Main.isSkillEnabled(event.getPlayer().getUniqueId(), SkillMeta.class)) {
			Arrow arrow = event.getPlayer().launchProjectile(Arrow.class, event.getPlayer().getEyeLocation().getDirection().multiply(3));
			arrow.setCritical(true);
			event.getPlayer().playSound(event.getPlayer().getEyeLocation(), Sound.ENTITY_ARROW_SHOOT, 1, 1);
			arrows.add(arrow);
		}
	}
	
	@EventHandler
	private void onProjectileHit(ProjectileHitEvent event) {
		arrows.remove(event.getEntity());
	}
	
	@EventHandler
	private void onProjectileHit(EntityDamageByEntityEvent event) {
		if(event.getDamager() instanceof Arrow && ((Arrow)event.getDamager()).getShooter() instanceof Player && Main.isSkillEnabled(((Player)((Arrow)event.getDamager()).getShooter()).getUniqueId(), SkillMeta.class)) {
			impacters.add(new Impact(event.getEntity().getLocation().add(0, 1, 0)));
		}
	}

	private static class Impact {
		private Location center;
		private int count;
		
		public Impact(Location center) {
			center.setPitch(0);
			this.center = center;
		}
		
		public boolean impact() {
			if(count++ == 0) {
				IntStream.range(0, 16).mapToObj(x -> cloneRange(360 / 16 * x)).forEach(loc -> {
					loc.getWorld().playSound(loc, Sound.BLOCK_IRON_TRAPDOOR_CLOSE, 1, 2);
					Vector vec = loc.getDirection();
					loc.getWorld().spawnParticle(Particle.END_ROD, loc, 0, vec.getX(), vec.getY(), vec.getZ(), 0.5);
				});
				center.getWorld().playSound(center, Sound.ENTITY_FIREWORK_SHOOT, 1, 2);
			}
			if(count > 10 && count < 20) {
				center.getWorld().playSound(center, Sound.BLOCK_CHORUS_FLOWER_GROW, 1, 1);
			}
			if(count > 20) {
				center.setYaw(center.getYaw() + 5);
				IntStream.range(0, 16).mapToObj(x -> cloneRange(360 / 16 * x)).map(x -> x.add(x.getDirection().multiply(30 - count).multiply(0.5))).forEach(loc -> {
					Vector vec = loc.getDirection().multiply(-1);
					loc.getWorld().spawnParticle(Particle.BLOCK_DUST, loc, 0, vec.getX(), vec.getY(), vec.getZ(), 1, new MaterialData(Material.REDSTONE_WIRE));
					loc.getWorld().getNearbyEntities(loc, 1, 1, 1)
						.stream()
						.filter(Creature.class::isInstance)
						.map(Creature.class::cast)
						.forEach(creature -> {
							creature.damage(5);
							creature.setLastDamageCause(null);
							loc.getWorld().playSound(loc, Sound.BLOCK_GLASS_BREAK, 0.5f, 2);
						});
					
				});
			}
			return count > 30;
		}
		
		private Location cloneRange(int yaw) {
			Location loc = center.clone();
			loc.setYaw(loc.getYaw() + yaw);
			return loc;
		}
	}
	
	private static class SkillMeta extends ArenaMetaValue {

		protected SkillMeta(Player player, int require, int cost) {
			super("斬裂矢装着中(左クリックで貯め無し発射)", player, require, cost);
		}
		
		@Override
		public void onSkillEnable() {
			
		}
		
		@Override
		public void onSkillEnd() {
			
		}
		
		@Override
		public boolean onTick() {
			Location loc = player.getLocation();
			loc.getWorld().spawnParticle(Particle.FLAME, loc.add(0, 0.5, 0), 1, 1, 0.5, 1, 0);
			return super.onTick();
		}
		
	}
}
