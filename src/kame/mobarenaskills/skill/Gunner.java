package kame.mobarenaskills.skill;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Silverfish;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

import kame.api.nbt.TagReader;
import kame.api.nbt.TagTool;
import kame.kameguns.baseutils.param.GunParam;
import kame.kameguns.weapon.Ballistics;
import kame.mobarenaskills.ArenaMetaValue;
import kame.mobarenaskills.Main;
import kame.mobarenaskills.kameguns.TurretBullet;

public class Gunner implements Listener {
	
	private static final Map<ArmorStand, SkillMeta> stands = new HashMap<>();
	public static ArenaMetaValue createMeta(Player player, int require, int cost) {
		return new SkillMeta(player, require, cost);
	}
	
	@EventHandler
	private void onSkillEnable(PlayerSwapHandItemsEvent event) {
		Player player = event.getPlayer();
		if(!Main.hasSkill(player.getUniqueId(), SkillMeta.class))return;
		if(!TagTool.hasTag(event.getMainHandItem(), "arenabook"))return;
		event.setCancelled(true);
		if(Main.isSkillEnabled(player.getUniqueId(), SkillMeta.class))return;
		Main.requestEnable(player, SkillMeta.class);
	}
	
	private static class SkillMeta extends ArenaMetaValue {
		private ArmorStand stand;
		private boolean shooting;
		private double speed;
		private double count;
		protected SkillMeta(Player player, int require, int cost) {
			super("§cオートタレット§f出現中…", player, require, cost);
		}
		
		public ArmorStand dropGun(ArmorStand stand) {
			stand.setGravity(true);
			stand.setSmall(true);
			stand.setArms(true);
			stand.setVisible(false);
			stand.setInvulnerable(true);
			stand.setVelocity(player.getEyeLocation().getDirection().multiply(0.3));
			stand.setHelmet(new ItemStack(Material.GOLD_HOE, 1, (short)10));
			stand.getEquipment().setItemInMainHand(new ItemStack(Material.GOLD_HOE, 1, (short)9));
			stand.setLeftArmPose(new EulerAngle(0, 0, Math.toRadians(90)));
			return this.stand = stand;
		}
		
		@Override
		public void onSkillEnable() {
			SkillMeta meta = Main.getSkill(player.getUniqueId(), SkillMeta.class);
			Location loc = player.getEyeLocation();
			ArmorStand stand = meta.dropGun(loc.getWorld().spawn(loc, ArmorStand.class));
			stands.put(stand, meta);
		}
		
		@Override
		public void onSkillEnd() {
			stands.remove(stand);
			stand.remove();
			speed = 0;
			count = 0;
		}
		
		@Override
		public boolean onTick() {
			if(stand != null) {
				Optional<Location> target = stand.getNearbyEntities(16, 16, 16)
					.stream()
					.filter(Creature.class::isInstance)
					.filter(x -> !(x instanceof Silverfish))
					.map(Creature.class::cast)
					.filter(Entity::isValid)
					.map(Creature::getEyeLocation)
					.sorted((x, y) -> Double.compare(x.distance(stand.getEyeLocation()), y.distance(stand.getEyeLocation())))
					.findFirst();
				if(shooting = target.isPresent()) {
					Location to =stand.getLocation().setDirection(target.get().toVector().subtract(stand.getEyeLocation().toVector()));
					to.setYaw(to.getYaw() + 90);
					stand.teleport(to);
				}else {
					addTime(1);
				}
				if(shooting) {
					speed = Math.min(50, speed + 2);
				}else {
					speed = Math.max(0, speed - 0.3);
				}
				Location loc = stand.getEyeLocation();
				loc.setYaw(loc.getYaw() - 90);
				Location l = loc.clone();
				l.setPitch(-45);
				loc.add(l.getDirection());
				if(speed == 50) {
					count += 6;
					String nbt = "{}";
					GunParam param = new GunParam(TagReader.parseNBT(nbt));
					Ballistics.shootBall(new TurretBullet(player, stand, loc.getDirection(), param));
					Vector vec = stand.getEyeLocation().getDirection().multiply(0.2);
					loc.getWorld().spawnParticle(Particle.BLOCK_DUST, l.add(0, 0.3, 0), 0, vec.getX(), 0.3, vec.getZ(), 1, new MaterialData(Material.GOLD_BLOCK));
					if(count % 4 == 0) {
						loc.getWorld().spawnParticle(Particle.EXPLOSION_NORMAL, l, 0, vec.getX(), 0.1, vec.getZ(), 1);
					}
					Bukkit.getScheduler().runTask(Main.instance, () -> {
						try {
							loc.getWorld().playSound(loc, Sound.ENTITY_PLAYER_BIG_FALL, 0.5f, 0.5f);
							loc.getWorld().playSound(loc, Sound.BLOCK_WOOD_BREAK, 1, 0.5f);
							Thread.sleep(10);
							loc.getWorld().playSound(loc, Sound.ENTITY_PLAYER_BIG_FALL, 0.5f, 0.5f);
							loc.getWorld().playSound(loc, Sound.BLOCK_WOOD_BREAK, 1, 0.5f);
							Thread.sleep(10);
							loc.getWorld().playSound(loc, Sound.ENTITY_PLAYER_BIG_FALL, 0.5f, 0.5f);
							loc.getWorld().playSound(loc, Sound.BLOCK_WOOD_BREAK, 1, 0.5f);
							Thread.sleep(10);
							loc.getWorld().playSound(loc, Sound.ENTITY_PLAYER_BIG_FALL, 0.5f, 0.5f);
							loc.getWorld().playSound(loc, Sound.BLOCK_WOOD_BREAK, 1, 0.5f);
						}catch(InterruptedException e) {
							e.printStackTrace();
						}
					
					});
					
				}else {
					count = Math.max(0, count - 4);
				}
				if(speed > 10) {
					loc.getWorld().playSound(loc, Sound.ENTITY_PLAYER_BREATH, 0.5f, (float)speed / 25);
				}
				if(count > 600 && count % 5 == 0) {
					loc.getWorld().spawnParticle(Particle.EXPLOSION_NORMAL, loc, 0, 0, 0, 0, 1);
					loc.getWorld().playSound(loc, Sound.BLOCK_REDSTONE_TORCH_BURNOUT, 0.2f, 0.5f);
				}
				if(count > 600) {
					loc.getWorld().playSound(loc, Sound.BLOCK_REDSTONE_TORCH_BURNOUT, 0.3f, 0.5f);
				}
				stand.setRightArmPose(new EulerAngle(0, stand.getRightArmPose().getY() + Math.toRadians(speed), Math.toRadians(loc.getPitch() - 90)));
			}
			return super.onTick();
		}
		
	}
}
