package kame.mobarenaskills.skill;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Creature;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.event.player.PlayerToggleSprintEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import kame.api.nbt.TagTool;
import kame.mobarenaskills.ArenaMetaValue;
import kame.mobarenaskills.Main;

public class Fighter implements Listener {
	
	static {
		Bukkit.getScheduler().runTaskTimer(Main.instance, Fighter::onTick, 1, 1);
	}
	
	private static Set<Slasher> slashers = new HashSet<>();
	private static void onTick() {
		slashers.removeIf(Slasher::slash);
	}

	public static ArenaMetaValue createMeta(Player player, int require, int cost) {
		return new SkillMeta(player, require, cost);
	}
	
	@EventHandler
	private void onSkillEnable(PlayerSwapHandItemsEvent event) {
		Player player = event.getPlayer();
		if(!Main.hasSkill(player.getUniqueId(), SkillMeta.class))return;
		if(!TagTool.hasTag(event.getMainHandItem(), "arenabook"))return;
		event.setCancelled(true);
		if(Main.isSkillEnabled(player.getUniqueId(), SkillMeta.class))return;
		Main.requestEnable(player, SkillMeta.class);
	}
	
	@EventHandler
	private void fighterSkill(PlayerToggleSprintEvent event) {
		if(!event.isSprinting())return;
		SkillMeta meta = Main.getSkill(event.getPlayer().getUniqueId(), SkillMeta.class);
		if(meta == null)return;
		Location loc = event.getPlayer().getLocation();
		loc.setPitch(-5);
		event.getPlayer().setVelocity(loc.getDirection().multiply(2));
		loc.getWorld().playSound(loc, Sound.ENTITY_PLAYER_ATTACK_SWEEP, 1, 0.5f);
		List<LivingEntity> capture = event.getPlayer().getNearbyEntities(10, 1, 10)
				.stream()
				.filter(Creature.class::isInstance)
				.map(LivingEntity.class::cast)
				.filter(LivingEntity::isValid)
				.collect(Collectors.toList());
		new BukkitRunnable() {
			int count;
			@Override
			public void run() {
				capture.removeIf(entity -> event.getPlayer().getLocation().distance(entity.getLocation()) < 3 && slashers.add(new Slasher(entity, 30, 2)));
				if(count++ > 10) { this.cancel(); }
			}
		}.runTaskTimer(Main.instance, 1, 1);
		event.setCancelled(true);
	}
	
	private static class Slasher {
		private LivingEntity entity;
		private int time;
		private int count;
		private double damage;
		
		public Slasher(LivingEntity entity, int time, double damage) {
			this.entity = entity;
			this.time = time;
			this.damage = damage;
		}
		
		public boolean slash() {
			switch(count++) {
			case 0:
				entity.damage(0);
				entity.setVelocity(new Vector(0, 1, 0));
				entity.getWorld().spawnParticle(Particle.SWEEP_ATTACK, entity.getEyeLocation(), 5);
				entity.getWorld().playSound(entity.getEyeLocation(), Sound.ENTITY_PLAYER_ATTACK_SWEEP, 1, 1.5f);
				break;
			case 10:
				entity.setGravity(false);
			case 12:
			case 14:
			case 16:
			case 18:
			case 20:
			case 25:
				Location loc = entity.getLocation().add(0, entity.getEyeHeight() / 2, 0);
				loc.getWorld().spawnParticle(Particle.SWEEP_ATTACK, loc, 1);
				loc.getWorld().playSound(loc, Sound.ENTITY_PLAYER_ATTACK_SWEEP, 1, 1.5f);
				entity.setNoDamageTicks(0);
				entity.damage(damage);
				entity.setVelocity(Vector.getRandom().multiply(0.2));
				break;
			case 26:
				entity.setGravity(true);
			default:
			}
			return count >= time;
		}
		
		@Override
		public int hashCode() {
			return entity.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			return entity.equals(obj);
		}
	}
	
	private static class SkillMeta extends ArenaMetaValue {

		protected SkillMeta(Player player, int require, int cost) {
			super("ダッシュで辻斬り", player, require, cost);
		}
		
		@Override
		public void onSkillEnable() {
			
		}
		
		@Override
		public void onSkillEnd() {
			
		}
		
		@Override
		public boolean onTick() {
			super.addTime(-1);
			Location loc = player.getLocation().add(0, 0.3, 0);
			loc.setPitch(0);
			Vector vec = loc.getDirection();
			loc.setYaw(loc.getYaw() - 90);
			
			for(int i = 0; i < 10; i++) {
				loc.add(vec);
				loc.getWorld().spawnParticle(Particle.CRIT_MAGIC, loc.clone().add(loc.getDirection()), 1, 0, 0, 0, 0);
				loc.getWorld().spawnParticle(Particle.CRIT_MAGIC, loc.clone().subtract(loc.getDirection()), 1, 0, 0, 0, 0);
			}
			return super.onTick();
		}
	}
}
