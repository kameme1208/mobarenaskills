package kame.mobarenaskills.skill;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.inventory.ItemStack;

import kame.api.nbt.TagTool;
import kame.mobarenaskills.ArenaMetaValue;
import kame.mobarenaskills.Main;

public class Tank implements Listener {

	public static ArenaMetaValue createMeta(Player player, int require, int cost) {
		return new SkillMeta(player, require, cost);
	}
	
	@EventHandler
	private void onSkillEnable(PlayerSwapHandItemsEvent event) {
		Player player = event.getPlayer();
		if(!Main.hasSkill(player.getUniqueId(), SkillMeta.class))return;
		if(!TagTool.hasTag(event.getMainHandItem(), "arenabook"))return;
		event.setCancelled(true);
		if(Main.isSkillEnabled(player.getUniqueId(), SkillMeta.class))return;
		Main.requestEnable(player, SkillMeta.class);
	}
	
	@EventHandler
	private void tankSkill(EntityDamageByEntityEvent event) {
		SkillMeta meta = Main.getSkill(event.getEntity().getUniqueId(), SkillMeta.class);
		if(meta == null)return;
		event.setDamage(0.01);
		World world = event.getDamager().getWorld();
		world.playSound(event.getDamager().getLocation(), Sound.ENTITY_IRONGOLEM_HURT, 1, 1);
		world.spawnParticle(Particle.END_ROD, event.getEntity().getLocation(), 10);
	}
	
	private static class SkillMeta extends ArenaMetaValue {
		
		private ArmorStand[] stands = new ArmorStand[10];
		private Location loc = new Location(null, 0, 0, 0);
		protected SkillMeta(Player player, int require, int cost) {
			super("§dダメージ無効化中", player, require, cost);
		}
		
		private ArmorStand createStand() {
			ArmorStand stand = player.getWorld().spawn(player.getLocation(), ArmorStand.class);
			stand.setVisible(false);
			stand.setMarker(true);
			stand.setSmall(true);
			stand.setGravity(false);
			stand.setInvulnerable(true);
			stand.setAI(true);
			stand.getEquipment().setHelmet(new ItemStack(Material.IRON_BLOCK));
			return stand;
		}
		
		@Override
		public void onSkillEnable() {
			for(int i = 0; i < 10; i++)stands[i] = createStand();
		}
		
		
		@Override
		public void onSkillEnd() {
			for(ArmorStand stand : stands)stand.remove();
		}
		
		@Override
		public boolean onTick() {
			loc.setYaw(loc.getYaw() + 10);
			for(ArmorStand stand : stands)stand.setFireTicks(10);
			for(int i = 0; i < 7; i++) {
				loc.setYaw(loc.getYaw() + 360 / 7);
				stands[i].teleport(player.getLocation().add(loc.getDirection().multiply(2)));
				stands[i].getNearbyEntities(.5, 1, .5).stream().filter(LivingEntity.class::isInstance).filter(x -> !(x instanceof Player))
				.map(LivingEntity.class::cast).forEach(this::damage3);
			}
			Location loc = this.loc.clone();
			loc.setYaw(loc.getYaw() * -1);
			for(int i = 7; i < 10; i++) {
				loc.setYaw(loc.getYaw() + 360 / 3);
				stands[i].teleport(player.getLocation().add(loc.getDirection().multiply(1.5)));
				stands[i].getNearbyEntities(.5, 1, .5).stream().filter(LivingEntity.class::isInstance).filter(x -> !(x instanceof Player))
				.map(LivingEntity.class::cast).forEach(this::damage5);
			}
			return super.onTick();
		} 
		
	}
}
