package kame.mobarenaskills;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import kame.kameguns.weapon.Spell;
import kame.kameplayer.baseutils.Utils;
import kame.kameplayer.baseutils.Utils.Fail;
import kame.mobarenaskills.kameguns.Explosion;
import kame.mobarenaskills.kameguns.Spread;
import kame.mobarenaskills.skill.Archer;
import kame.mobarenaskills.skill.Chemist;
import kame.mobarenaskills.skill.Fighter;
import kame.mobarenaskills.skill.Gunner;
import kame.mobarenaskills.skill.Tank;
import kame.mobarenaskills.skill.Witch;

public class Main extends JavaPlugin {
	
	public static Plugin instance;

	private static Map<UUID, ArenaMetaValue> enable = new HashMap<>();
	private static Map<UUID, ArenaMetaValue> values = new HashMap<>();
	
	public static final String SKILL = "EnableMobarenaSkill";
	
	public static boolean hasSkill(UUID uid, Class<? extends ArenaMetaValue> skillClass) {
		return skillClass.isInstance(values.get(uid));
	}

	public static boolean isSkillEnabled(UUID uid, Class<? extends ArenaMetaValue> skillClass) {
		return skillClass.isInstance(enable.get(uid));
	}
	
	public static void requestEnable(Player player, Class<? extends ArenaMetaValue> skillClass) {
		if(player.getLevel() < 10) { return; }
		player.setLevel(player.getLevel() - 5);
		ArenaMetaValue meta = values.get(player.getUniqueId());
		if(skillClass.isInstance(meta)) {
			enable.put(player.getUniqueId(), meta);
			meta.enable();
			player.getWorld().playSound(player.getLocation(), Sound.ENTITY_WITHER_SPAWN, 1, 1);
		}
	}
	
	public static <T extends ArenaMetaValue> T getSkill(UUID uid, Class<T> skillClass) {
		ArenaMetaValue meta = enable.get(uid);
		return skillClass.isInstance(meta) ? skillClass.cast(meta) : null;
	}
	
	@Override
	public void onEnable() {
		instance = this;
		getCommand("arena").setExecutor(this);
		getCommand("arena").setTabCompleter(this);
		Bukkit.getPluginManager().registerEvents(new Archer() , this);
		Bukkit.getPluginManager().registerEvents(new Chemist(), this);
		Bukkit.getPluginManager().registerEvents(new Fighter(), this);
		Bukkit.getPluginManager().registerEvents(new Gunner() , this);
		Bukkit.getPluginManager().registerEvents(new Tank()   , this);
		Bukkit.getPluginManager().registerEvents(new Witch()  , this);
		
		Bukkit.getScheduler().runTaskTimer(this, this::onTick, 1, 1);
		
		Spell.register("Spread", new Spread());
		Spell.register("Explosion", new Explosion());
		
	}
	
	@Override
	public void onDisable() {
		enable.values().forEach(ArenaMetaValue::end);
		enable.values().clear();
	}
	
	private void onTick() {
		enable.values().removeIf(ArenaMetaValue::onTick);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(args.length < 2)return Utils.sendFailCommand(sender, Fail.LowParam, "/arena <player> set|remove [SkillType]");
		Player player = Bukkit.getPlayerExact(args[0]);
		if(player == null)return Utils.sendFailCommand(sender, Fail.NoPlayer, args[0]);
		if(!args[1].matches("set|remove"))return Utils.sendFailCommand(sender, Fail.NoEnum, args[1]);
		if(args[1].equals("set")) {
			if(values.containsKey(player.getUniqueId()))return Utils.sendFailCommand(sender, Fail.Other, "そのプレイヤーは既にスキルを持っています");
			if(args.length < 3)return Utils.sendFailCommand(sender, Fail.LowParam, "/arena <player> set [SkillType]");
			if(!args[2].toUpperCase().matches("FIGHTER|TANK|ARCHER|CHEMIST|GUNNER|WITCH"))return Utils.sendFailCommand(sender, Fail.NoEnum, args[2]);
			int require = args.length > 3 ? Utils.parse(args[3], 10) : 10;
			int cost = args.length > 4 ? Utils.parse(args[4], 5) : 5;
			switch(args[2].toUpperCase()) {
			case "FIGHTER":
				values.put(player.getUniqueId(), Fighter.createMeta(player, require, cost));
				break;
			case "TANK":
				values.put(player.getUniqueId(), Tank.createMeta(player, require, cost));
				break;
			case "ARCHER":
				values.put(player.getUniqueId(), Archer.createMeta(player, require, cost));
				break;
			case "CHEMIST":
				values.put(player.getUniqueId(), Chemist.createMeta(player, require, cost));
				break;
			case "GUNNER":
				values.put(player.getUniqueId(), Gunner.createMeta(player, require, cost));
				break;
			case "WITCH":
				values.put(player.getUniqueId(), Witch.createMeta(player, require, cost));
				break;
			}
			sender.sendMessage("[kame.] set to Skill[" + args[2] + "] from " + player.getName());
		}else {
			if(values.containsKey(player.getUniqueId())) {
				values.remove(player.getUniqueId()).end();
				sender.sendMessage("[kame.] remove to Skills from " + player.getName());
			}else {
				return Utils.sendFailCommand(sender, Fail.Other, "そのプレイヤーはスキルを持っていません");
			}
		}
		
		return true;
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		if(args.length == 1) return null; 
		if(args.length == 2) return Arrays.asList("set", "remove").stream().filter(x -> x.startsWith(args[1])).collect(Collectors.toList());
		if(args.length == 3) return Arrays.asList("Archer", "Chemist", "Fighter", "Gunner", "Tank").stream().filter(x -> x.toLowerCase().startsWith(args[2].toLowerCase())).collect(Collectors.toList());
		return new ArrayList<>();
		
	}
	//pull @a[x=-346,y=80,z=1289,r=8,score_skill=5,score_skill_min=5,score_trig_min=5,score_trig=5,m=2] minecraft:enchanted_book 1 0 {arenabook:1b,display:{Name:"§b§lSkillBook§r", Lore:["§5[§3業種説明§5]§R","§a散弾を発射して複数の敵に強力なダメージを与える","§4ATK§8:§6★★★★★★★★§7","§2DEF§8:§6★§7☆☆☆☆☆☆☆","§dSUP§8:§6★★★★§7☆☆☆☆","","§5[§3スキル説明§5]","§6発動条件§8:§a10LV以上§7(コスト§9: §65LV§7)§r","§6発動手順§8:§bSkillBook§aを§eF§aキー","§6持続時間§8:§e60§a秒(ターゲット時)","§c周囲の敵を一掃するタレットを召喚する","§cその威力に圧倒され誰も手が出ない"]}}
	//pull @a[x=-346,y=80,z=1289,r=8,score_skill=4,score_skill_min=4,score_trig_min=4,score_trig=4,m=2] minecraft:enchanted_book 1 0 {arenabook:1b,display:{Name:"§b§lSkillBook§r", Lore:["§5[§3業種説明§5]§R","§a属性攻撃やポーションを駆使して回復や攻撃を行う","§4ATK§8:§6★★★§7☆☆☆☆☆","§2DEF§8:§6★★★§7☆☆☆☆☆","§dSUP§8:§6★★★★★★§7☆☆","","§5[§3スキル説明§5]","§6発動条件§8:§a10LV以上§7(コスト§9: §65LV§7)§r","§6発動手順§8:§bSkillBook§aを§eF§aキー","§6持続時間§8:§e60§a秒","§c周囲を回るプレートを操って攻撃する","§cそのフレートに触れる物は裂傷を負う"]}}
	//pull @a[x=-346,y=80,z=1289,r=8,score_skill=3,score_skill_min=3,score_trig_min=3,score_trig=3,m=2] minecraft:enchanted_book 1 0 {arenabook:1b,display:{Name:"§b§lSkillBook§r", Lore:["§5[§3業種説明§5]§R","§a強力な矢を使って安定した火力の遠距離攻撃を行う","§4ATK§8:§6★★★★★★§7☆☆","§2DEF§8:§6★§7☆☆☆☆☆☆☆","§dSUP§8:§6★★★§7☆☆☆☆☆","","§5[§3スキル説明§5]","§6発動条件§8:§a10LV以上§7(コスト§9: §65LV§7)§r","§6発動手順§8:§bSkillBook§aを§eF§aキー","§6持続時間§8:§e60§a秒","§c敵に当たる周囲を斬裂する矢を装填する","§c周囲にいる敵は皆切り刻まれるだろう…"]}}
	//pull @a[x=-346,y=80,z=1289,r=8,score_skill=2,score_skill_min=2,score_trig_min=2,score_trig=2,m=2] minecraft:enchanted_book 1 0 {arenabook:1b,display:{Name:"§b§lSkillBook§r", Lore:["§5[§3業種説明§5]§R","§a頑丈な武具と体で身を守り味方の盾となり援護する","§4ATK§8:§6★§7☆☆☆☆☆☆☆","§2DEF§8:§6★★★★★★★★§7","§dSUP§8:§6★★★§7☆☆☆☆☆","","§5[§3スキル説明§5]","§6発動条件§8:§a10LV以上§7(コスト§9: §65LV§7)§r","§6発動手順§8:§bSkillBook§aを§eF§aキー","§6持続時間§8:§e60§a秒","§cダメージを無効化する鋼の輪を作る","§cこの中に入る敵はその鋼に押し潰される"]}}
	//pull @a[x=-346,y=80,z=1289,r=8,score_skill=1,score_skill_min=1,score_trig_min=1,score_trig=1,m=2] minecraft:enchanted_book 1 0 {arenabook:1b,display:{Name:"§b§lSkillBook§r", Lore:["§5[§3業種説明§5]§R","§a自慢の身の軽さでフィールドを駆け回り攻撃を行う","§4ATK§8:§6★★★★§7☆☆☆☆","§2DEF§8:§6★★★★★§7☆☆☆","§dSUP§8:§6★★★§7☆☆☆☆☆","","§5[§3スキル説明§5]","§6発動条件§8:§a10LV以上§7(コスト§9: §65LV§7)§r","§6発動手順§8:§bSkillBook§aを§eF§aキー","§6持続時間§8:§e30§a秒","§c精神を集中させ一気に駆け抜けダメージを与える","§cその様はまさに通り魔のようだ…"]}}
}
