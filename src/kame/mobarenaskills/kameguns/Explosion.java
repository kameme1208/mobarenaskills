package kame.mobarenaskills.kameguns;

import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;

import kame.kameguns.weapon.Spell;
import kame.kameguns.weapon.projectile.Collision;
import kame.kameplayer.baseutils.Utils;

public class Explosion extends Spell {

	@Override
	public void call(Entity shooter, Collision pos, String label, String[] args) {
		double r = Utils.parseRaw(args.length > 0 ? args[0] : "5", 5);
		double d = Utils.parseRaw(args.length > 1 ? args[1] : "5", 5);
		pos.getLocation().getWorld().spawnParticle(Particle.EXPLOSION_LARGE, pos.getLocation(), 2);
		pos.getLocation().getWorld().spawnParticle(Particle.EXPLOSION_NORMAL, pos.getLocation(), 5, 0, 0, 0, 0.2);
		pos.getLocation().getWorld().spawnParticle(Particle.FLAME, pos.getLocation(), 5, 0, 0, 0, 0.1);
		pos.getLocation().getWorld().playSound(pos.getLocation(), Sound.ENTITY_GENERIC_EXPLODE, 0.5f, 1);
		for(Entity e : pos.getLocation().getWorld().getNearbyEntities(pos.getLocation(), r, r, r)) {
			if(e instanceof Creature) {
				((Creature)e).damage(d, shooter);
			}
		}
	}

}
