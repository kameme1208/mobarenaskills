package kame.mobarenaskills.kameguns;

import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import kame.api.collision.CollisionPosition;
import kame.kameguns.baseutils.param.GunParam;
import kame.kameguns.weapon.projectile.simple.BallEntityGun;

public class TurretBullet extends BallEntityGun {

	protected final ArmorStand stand;
	public TurretBullet(Player shooter, ArmorStand stand, Vector motion, GunParam param) {
		super(shooter, stand.getEyeLocation(), motion, param);
		this.stand = stand;
		noCollision.add(stand);
	}

	@Override
	protected boolean entityHit(CollisionPosition pos) {
		LivingEntity entity = (LivingEntity) pos.getEntity();
		player.playSound(player.getLocation(), Sound.ENTITY_ARROW_HIT, 0.5f, 2);
		entity.setNoDamageTicks(0);
		entity.damage(motion.length() * damage * 0.0625, stand);
		pos.getLocation().getWorld().spawnParticle(Particle.BLOCK_CRACK, pos.getLocation(), 16, 0, 0, 0, 0.1, REDSTONE);
		return result(pos, entity);
	}

}
