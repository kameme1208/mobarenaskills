package kame.mobarenaskills.kameguns;

import java.util.Random;

import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import kame.api.nbt.TagBuffer;
import kame.kameguns.baseutils.param.GunParam;
import kame.kameguns.weapon.Ballistics;
import kame.kameguns.weapon.Spell;
import kame.kameguns.weapon.projectile.Collision;
import kame.kameguns.weapon.projectile.simple.BallEntityGun;
import kame.kameplayer.baseutils.Utils;

public class Spread extends Spell {
	
	public Random rnd = new Random();

	@Override
	public void call(Entity shooter, Collision pos, String label, String[] args) {
		GunParam param = new GunParam(new TagBuffer().toReader());
		param.obj = new ItemStack(Material.STONE_BUTTON);
		param.speed = Double.parseDouble(args.length > 1 ? args[1] : "0.1");
		param.spells.add("Explosion:" + Utils.parse(args.length > 2 ? args[2] : "4", 4) + ":" + Utils.parse(args.length > 3 ? args[3] : "5", 5) + "");
		pos.getLocation().getWorld().spawnParticle(Particle.EXPLOSION_LARGE, pos.getLocation(), 2);
		pos.getLocation().getWorld().spawnParticle(Particle.EXPLOSION_NORMAL, pos.getLocation(), 5, 0, 0, 0, 0.2);
		pos.getLocation().getWorld().spawnParticle(Particle.FLAME, pos.getLocation(), 5, 0, 0, 0, 0.1);
		pos.getLocation().getWorld().playSound(pos.getLocation(), Sound.ENTITY_GENERIC_EXPLODE, 0.5f, 1);
		for(int i = Integer.parseInt(args.length > 0 ? args[0] : "8"); i > 0; i--) {
			Ballistics.shootBall(new BallEntityGun((Player)shooter, pos.getLocation().add(0, 0.1, 0), getRandom().add(new Vector(0, 1.5, 0)).multiply(0.1), param));
		}
	}
	
	public Vector getRandom() {
		return new Vector(rnd.nextGaussian(), rnd.nextGaussian(), rnd.nextGaussian());
	}
}
