package kame.mobarenaskills;

import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public abstract class ArenaMetaValue {

	public final Player player;
	private final BossBar bar;
	protected int enable;
	private int required;
	private int cost;
	protected ArenaMetaValue(String title, Player player, int require, int cost) {
		this.player = player;
		this.bar = Bukkit.createBossBar(title, BarColor.BLUE, BarStyle.SOLID);
		this.bar.addPlayer(player);
		this.bar.setVisible(false);
		this.required = require;
		this.cost = cost;
	}
	
	public abstract void onSkillEnable();
	
	public abstract void onSkillEnd();
	
	public boolean onTick() {
		bar.setProgress(Math.max(0, Math.min(enable-- / 600.0, 1)));
		if(enable <= 0) { end(); }
		return enable <= 0;
	}
	
	public void addTime(int i) {
		enable += i;
	}
	
	public final void enable() {
		enable = 600;
		bar.setProgress(1);
		bar.setVisible(true);
		onSkillEnable();
	}
	
	public final void end() {
		enable = 0;
		bar.setVisible(false);
		onSkillEnd();
	}
	
	public int getRequiredLevel() {
		return required;
	}
	
	public int getCostLevel() {
		return cost;
	}
	
	protected final void damage(double damage, LivingEntity target) {
		target.damage(damage, player);
		target.setLastDamageCause(null);
	}

	protected final void damage1(LivingEntity target) { damage(1, target); }
	protected final void damage2(LivingEntity target) { damage(2, target); }
	protected final void damage3(LivingEntity target) { damage(3, target); }
	protected final void damage4(LivingEntity target) { damage(4, target); }
	protected final void damage5(LivingEntity target) { damage(5, target); }
	
}
